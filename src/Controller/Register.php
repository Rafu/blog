<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\RegisterType;

class Register extends Controller
{
    /**
     * @Route("/register", name="register")
     */
    public function index(Request $req, UserPasswordEncoderInterface $encoder)
    {
        $user =new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()) {
            $encoded = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encoded);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute("home");

        }

        return $this->render('register/register.html.twig', [
            'controller_name' => 'Register',
            'form'=>$form->createView()
        ]);
    }
}
