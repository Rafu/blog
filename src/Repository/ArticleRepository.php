<?php

namespace App\Repository;

use App\Entity\Article;




class ArticleRepository{
    private $connection;

    public function __construct()
    {
        try {

            $this->connection = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch (\PDOException $e) {

            dump($e);

        }
    }

    private function fetch(string $query, array $params = []){
        try {
            $query = $this->connection->prepare($query);

            foreach ($params as $param => $value) {
                $query->bindValue($param, $value);
            }
    
            $query->execute();

            $result = [];

            foreach ($query->fetchAll() as $row) {
                $result[] = Article::fromSQL($row);
            }

            // if (count($result) <= 1) {
            //     return $result[0];
            // }

            return $result;

        } catch (\PDOException $e) {
            dump($e);
        }

    }

    public function add(Article $article)
    {
        $this->fetch(
            "INSERT INTO article (id, title, content, tag, url) VALUES (:id, :title, :content, :tag, :url)",
            [
                ":id" => $article->id,
                ":title" => $article->title,
                ":content" => $article->content,
                ":tag" => $article->tag,
                ":url" => $article->url
            ]
            );

        $article->id = intval($this->connection->lastInsertId());

        return $article;
    }

    public function update(Article $article) {
        try {

            $this->fetch(
            "UPDATE article SET title=:title, content=:content, tag=:tag, url=:url WHERE id=:id",
            [
            ":id" => $article->id,
            ":title" => $article->title,
            ":content" => $article->content,
            ":tag" => $article->tag,
            ":url" => $article->url,
            ]
            );

            return $article;

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function delete(int $id){
        try {

            $this->fetch(
            "DELETE FROM article WHERE id=:id",
            [
            ":id" => $id,
            ]
            );
            return true;

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;

    }

    public function get(int $id)
    {
        return $this->fetch("SELECT * FROM article WHERE id=:id", [":id" => $id]);
    }

    public function getAll()
    {
        $result = $this->fetch("SELECT * FROM article");
        // dump($result);
        return $result;
    }


}

