<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Nom:',
                )))
            ->add('email', null, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'E-mail:',
                )))
            ->add('password', RepeatedType::class, [
                'type'=>PasswordType::class,
                'invalid_message'=>"the password must watch",
                'options' => array('attr' => array(
                'class' => 'password-field' ,
                'placeholder' => 'Password:')),
                'required' => true,
                'first_options' => array('label' => false),
                'second_options' => array('label' => false, 
                'attr' => array('placeholder' => 'Repeat Password:'))
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
