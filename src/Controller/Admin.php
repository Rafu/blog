<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use App\Form\ArticleType;

class Admin extends Controller
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(Request $req, ArticleRepository $repo)
    {
        $article = new Article();


        $form = $this->createFormBuilder($article)
            ->add('title', TextType::class, array('attr' => array(
                'placeholder' => 'Title'
           )))
            ->add('url', FileType::class, [])
            ->add('content', CKEditorType::class, array('label' => false, 'config' => ['toolbar' => 'full', 'height' => 300]))
            ->add('tag', TextType::class, array('label' => false, 'attr' => array(
                'placeholder' => 'Tag'
           ) ))
            ->add('submit', SubmitType::class)
            ->getForm();

            $form->handleRequest($req);


            if ($form->isSubmitted() && $form->isValid()){
                $article = $form->getData();

                
                $repo->add($this->imagePath($article));
            }
            

            // dump($article);
        return $this->render('admin/admin.html.twig', [
            'controller_name' => 'Admin',
            "articleform"=>$form->createView(),
            "article" => $article,
        ]);
    }
    
    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
    // public function redirect(ArticleRepository $article){
    //     $article->add();
    // }

    public function imagePath($article)
    {
        $fileName = $this->generateUniqueFileName().'.'.$article->url->guessExtension();            
        $article->url->move(dirname(__FILE__)."/../../public/upload", $fileName);
        $article->url = "upload/".$fileName;
        return $article;
    }

}
