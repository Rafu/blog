<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use App\Form\ArticleType;
use App\Controller\Admin;

class Edition extends Controller
{
  /**
     * @Route("/admin/{id}", name="edition")
     */
public function index(int $id , ArticleRepository $repo, Request $request)
    {
        $article = $repo->get($id);
        $article = $article[0];

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $article = $form->getData();

            $admin = new Admin;
            $admin->imagePath($article);
            
            $repo->update($article);
            
            // $fileName = $this->generateUniqueFileName().'.'.$article->url->guessExtension();            
            // $article->url->move(dirname(__FILE__)."/../../public/upload", $fileName);
            // $article->url = "upload/".$fileName;
            // $repo->add($article);

            return $this->redirectToRoute("home");
        }

        return $this->render('admin/edition.html.twig', [
            "form" => $form->createView(),
            "article" => $article
        ]);
    }



    // public function update(int $id, ArticleRepository $repo, Request $req){
    //     $update = $repo->get($id);
    //     $form = $this->createForm(ArticleType::class, $update);

    //     $form->handleRequest($req);
    //     if ($form->isSubmitted() && $form->isValid()){
    //         $article = $form->getData();
    //         return $this->redirectToRoute("admin");
            
    //         return $this->render('admin.html.twig', [
    //             "form"=>$form->createView(),
    //             "update" => $update
    //         ]);
    //     }

    // }

  }