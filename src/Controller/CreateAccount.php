<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CreateAccount extends Controller
{
    /**
     * @Route("/create-account", name="create-account")
     */
    public function index()
    {
        return $this->render('create-account/create-account.html.twig', [
            'controller_name' => 'createaccount',
        ]);
    }
}
