<?php

namespace App\Repository;

use App\Entity\User;




class UserRepository{
  public function getAll() : array{
    $articles = [];
    try {

        $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

        $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        $query = $cnx->prepare("SELECT * FROM user");

        $query->execute();


        foreach ($query->fetchAll() as $row) {
            $user = new User();
            $user->fromSQL($row);
            $users[] = $user;
        }

    } catch (\PDOException $e) {
        dump($e);
    }
    return $users;
}

// public function add(SmallDog $dog)
// {

//     try {
//         $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

//         $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

//         $query = $cnx->prepare("INSERT INTO small_dog (name, breed, age) VALUES (:name, :breed, :age)");
        
//         $query->bindValue(":name", $dog->name);
//         $query->bindValue(":breed", $dog->breed);
//         $query->bindValue(":age", $dog->age);

//         $query->execute();

//         $dog->id = intval($cnx->lastInsertId());

//     } catch (\PDOException $e) {
//         dump($e);
//     }
// }

}

