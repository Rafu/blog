<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $roles;


    /*
        le constructeur sert à definir le rôle
    */

    public function __construct()
    {
        $this->roles = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRoles()
    {
        if($this->roles) {
            return ["ROLE_ADMIN"];
        }
        else {
            return ["ROLE_USER"];
        }
    }

    public function setRoles(bool $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    public function getSalt()
    {
    }

    public function getUsername(): ?string
    {
        return $this->name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    function eraseCredentials()
    {
        
    }


}




// namespace App\Entity;

// class User {
//   public $id;
//   public $name;
//   private $password;
//   public $email;

//   public function _construct(int $id, string $name, string $password, string $email) {

//     $this->id = $id;
//     $this->name = $name;
//     $this->password = $password;
//     $this->email = $email;
//   }
//   public static function fromSQL(array $rawData) {
//     return new User(
//         $rawData["id"],
//         $rawData["name"],
//         $rawData["password"],
//         $rawData["email"]
        
//     );
// }

// }