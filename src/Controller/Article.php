<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;

class Article extends Controller
{
    /**
     * @Route("/article", name="article")
     */
    public function index(ArticleRepository $repo)
    {
        $idret = $repo->get($_GET["id"]);
        // dump($idret);
        return $this->render('article/article.html.twig', [
            'controller_name' => 'Article',
            'row' => $idret
        ]);
    }
}
