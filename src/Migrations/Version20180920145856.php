<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180920145856 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE article');
        $this->addSql('ALTER TABLE user ADD roles TINYINT(1) NOT NULL, CHANGE name name VARCHAR(255) NOT NULL, CHANGE password password VARCHAR(255) NOT NULL, CHANGE email email VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(45) DEFAULT NULL COLLATE latin1_swedish_ci, content LONGTEXT DEFAULT NULL COLLATE latin1_swedish_ci, tag VARCHAR(45) DEFAULT NULL COLLATE latin1_swedish_ci, url VARCHAR(45) DEFAULT NULL COLLATE latin1_swedish_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user DROP roles, CHANGE name name VARCHAR(45) DEFAULT NULL COLLATE latin1_swedish_ci, CHANGE email email VARCHAR(45) DEFAULT NULL COLLATE latin1_swedish_ci, CHANGE password password VARCHAR(255) DEFAULT NULL COLLATE latin1_swedish_ci');
    }
}
