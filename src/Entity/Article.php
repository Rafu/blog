<?php

namespace App\Entity;

class Article {
  public $id;
  public $title;
  public $content;
  public $tag;
  public $url;

  public function __construct(int $id = null, string $title = null, string $content = null, string $tag = null, string $url = null) {
    
    $this->id = $id;
    $this->title = $title;
    $this->content = $content;
    $this->tag = $tag;
    $this->url = $url;

    
  }

  public static function fromSQL(array $rawData) {
    return new Article(
        $rawData["id"],
        $rawData["title"],
        $rawData["content"],
        $rawData["tag"],
        $rawData["url"]
        
    );
}

}